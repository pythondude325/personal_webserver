/* eslint-env node */

const coffeescript = require("coffeescript")
const makeTransformer = require("../makeTransformer.js")

let coffeeScriptTransformer = makeTransformer(async function(input, sendStream, config){
  let result = coffeescript.compile(input.toString(), {
    sourceMap: true,
    filename: sendStream.parsedUrl.pathname,
    header: true,
    transpile: { plugins: ["@babel/plugin-transform-react-jsx"] }
  })

  if(sendStream.hasQueryParam("map")){
    console.log(result.v3SourceMap)
    return result.v3SourceMap
  } else {
    return result.js + "\n//# sourceMappingURL=" + sendStream.parsedUrl.pathname + "?map"
  }
}, "text/javascript")

module.exports = {
  ".coffee": coffeeScriptTransformer
  // Doesn't work yet: ".litcoffee": coffeeScriptTransformer
}
